package com.example.contactos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.contactos.databinding.ActivityMainBinding;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, ContactsAdapter.OnItemClickListener {

    ArrayList<Contact> contacts;
    private ActivityMainBinding binding;
    private ContactsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // ...
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        // Initialize contacts
        contacts = Contact.createContactsList(10);
        // Create adapter passing in the sample user data
        adapter = new ContactsAdapter(contacts);
        // Attach the adapter to the recyclerview to populate items
        binding.rvContacts.setAdapter(adapter);
        // Set layout manager to position the items
        binding.rvContacts.setLayoutManager(new LinearLayoutManager(this));

        binding.button.setOnClickListener(this);
        adapter.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // record this value before making any changes to the existing list
        int curSize = adapter.getItemCount();
        // Add a new contact
        // contacts.add(0, new Contact("Barney", true));
        contacts.add(curSize, new Contact("Barney", true));
        // Notify the adapter that an item was inserted at position 0
        adapter.notifyItemInserted(curSize);
    }

    @Override
    public void onItemClick(View itemView, int position) {
        String name = contacts.get(position).getName();
        Toast.makeText(this, name + " was clicked!", Toast.LENGTH_SHORT).show();
    }
}